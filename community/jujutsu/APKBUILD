# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=jujutsu
pkgver=0.17.0
pkgrel=1
pkgdesc="Git-compatible distributed version control system"
url="https://github.com/martinvonz/jj"
# armhf, armv7, x86: tests fail
arch="all !armhf !armv7 !x86"
license="Apache-2.0"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	libssh-dev
	openssl-dev
	zstd-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/martinvonz/jj/archive/v$pkgver/jujutsu-$pkgver.tar.gz"
builddir="$srcdir/jj-$pkgver"

export LIBGIT2_NO_VENDOR=1
export LIBSSH2_SYS_USE_PKG_CONFIG=1
export ZSTD_SYS_USE_PKG_CONFIG=1

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --bin jj

	for shell in bash fish zsh; do
		./target/release/jj util completion --$shell > jj.$shell
	done

	./target/release/jj util mangen > jj.1
}

check() {
	# test_gc: flaky on s390x, test_ssh_signing: no such file/directory
	if [ "$CARCH" = "s390x" ]; then
		cargo test --frozen --workspace -- \
			--skip test_git_backend::test_gc \
			--skip test_ssh_signing
	else
		cargo test --frozen --workspace -- \
			--skip test_ssh_signing
	fi
}

package() {
	install -Dvm755 target/release/jj -t "$pkgdir"/usr/bin/

	install -Dvm644 jj.bash \
		"$pkgdir"/usr/share/bash-completion/completions/jj
	install -Dvm644 jj.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/jj.fish
	install -Dvm644 jj.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_jj

	install -Dvm644 jj.1 -t "$pkgdir"/usr/share/man/man1/
	install -dvm755 "$pkgdir"/usr/share/doc
	cp -av docs "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
1e5023b2a66717b8839a82f57e99f1b97b910663a93baf98e26f3f53cc9bc3876f9dab2ff686254a782912b313748e812d1e0b573ab10eb6d25ecf9232e8066e  jujutsu-0.17.0.tar.gz
"
