# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=opentofu
pkgver=1.7.0
pkgrel=0
pkgdesc="OpenTofu lets you declaratively manage your cloud infrastructure"
url="https://opentofu.org"
arch="all"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/opentofu/opentofu/archive/refs/tags/v${pkgver/_/-}.tar.gz"
builddir="$srcdir/$pkgname-${pkgver/_/-}"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="-X 'github.com/opentofu/opentofu/version.dev=no'"
	go build -ldflags "$ldflags" ./cmd/tofu
}

check() {
	go test -v ./...
}

package() {
	install -Dm0755 tofu -t "$pkgdir"/usr/bin/
}

sha512sums="
5454310ab21f7b8dde49f56827970487bafaad2b4073f8fb3ba4e7b5c9a94a7f048db9b961a23387397d12a79d6385e91126b27d1bb654e25827a51f959f15ac  opentofu-1.7.0.tar.gz
"
