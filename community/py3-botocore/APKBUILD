# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-botocore
# Verify required version from py3-boto3 on this package before upgrading
pkgver=1.34.39
pkgrel=1
pkgdesc="The low-level, core functionality of Boto3"
url="https://github.com/boto/botocore"
arch="noarch"
license="Apache-2.0"
depends="
	py3-certifi
	py3-dateutil
	py3-jmespath
	py3-urllib3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/botocore/botocore-$pkgver.tar.gz"
builddir="$srcdir/botocore-$pkgver"
options="!check" # take way too long

replaces=py-botocore # Backwards compatibility
provides=py-botocore=$pkgver-r$pkgrel # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
5b3703f13167ea0a6b1483c6c7a1b446e417b84254cb8e86a682bdacd6d544f63846096d43e3e5b24512acdecd1e48ba341d204c1e0e38776ae72e13d14fae5c  botocore-1.34.39.tar.gz
"
