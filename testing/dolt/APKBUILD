# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=dolt
pkgver=1.35.13
pkgrel=0
pkgdesc="Dolt – It's Git for Data"
url="https://www.dolthub.com"
arch="all !x86 !armhf !armv7" # fails on 32-bit
license="Apache-2.0"
options="!check chmod-clean net"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/dolthub/dolt/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/go"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir -p build
	go build \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\"" \
		-o build \
		./cmd/...
}

package() {
	install -Dm755 build/dolt "$pkgdir"/usr/bin/dolt
}

sha512sums="
242df57fb843d075d0d49d13b0080dea6db29ff746ce5c9c79ddfe578a12c84d132839be72d49a0532d58b3e5b73030cb57b928643c5dd6908b460fd1a74bd64  dolt-1.35.13.tar.gz
"
