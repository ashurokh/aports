# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=level-zero
pkgver=1.16.15
pkgrel=0
pkgdesc="oneAPI Level Zero Loader"
url="https://spec.oneapi.com/versions/latest/elements/l0/source/index.html"
arch="all"
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/level-zero/archive/v$pkgver.tar.gz"
options="!check" # no testsuite

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=ON \
		-DCMAKE_BUILD_TYPE=None

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove 'usr/lib/libze_tracing_layer.so*'
	amove 'usr/lib/libze_validation_layer.so*'
}

sha512sums="
37f1770f1bfa71fa08d56afed53a4dd1107ff36d8790f7bb0f921237d9d0e90076ef22349e8f289cf8f154ca0e1e0fff6a7dcd55b71468989322b9892962b874  level-zero-1.16.15.tar.gz
"
