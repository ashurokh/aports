# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=cargo-deny
pkgver=0.14.23
pkgrel=0
pkgdesc="Cargo plugin for linting your dependencies"
url="https://github.com/EmbarkStudios/cargo-deny"
arch="x86_64 ppc64le" # x86 tests fail, the rest don't build due to a proc-macro in clap_derive v4.4.7
license="MIT OR Apache-2.0"
makedepends="
	cargo
	cargo-auditable
"
source="$pkgname-$pkgver.tar.gz::https://github.com/EmbarkStudios/cargo-deny/archive/refs/tags/$pkgver.tar.gz"
options="net"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	# These tests require the advisory repos to be cloned via git
	cargo test --frozen -- --skip fails_on_stale_advisory_database \
		--skip fetches_with_gix \
		--skip fetches_with_git \
		--skip downgrades_lint_levels \
		--skip detects_unsound \
		--skip warns_on_index_failures \
		--skip warns_on_ignored_and_withdrawn \
		--skip detects_vulnerabilities \
		--skip detects_unmaintained
}

package() {
	install -Dm 755 target/release/cargo-deny "$pkgdir"/usr/bin/cargo-deny

	for l in APACHE MIT
	do
		install -Dm 644 LICENSE-"$l" "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE-"$l"
	done
}

sha512sums="
072f61312fa8bf6dfe7103854132a885df9566c82ab04ab778a756b88a6901890686cadde9d0d9a4049d443dd2ecad4e283ac5a7591b1147218cc1b8a77518e0  cargo-deny-0.14.23.tar.gz
"
